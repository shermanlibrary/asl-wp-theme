<?php 	
	$form_action = 'http://novacat.nova.edu/search/X/';
	$input_name		=	'search'; 
?>

<!-- Hero Search
======================
-->	<section class="search hero" id="search-widget">

		<div class="wrap clearfix">

			<form class="align-left" method="get" role="search" name="novacat_search" id="novacat_search" action="<?php echo $form_action; ?>">
				<ul>
					
					<div class="fourcol first">
						<label class="label">Search the Catalog</label>
						<br> or try searching: 
						<a class="hide-text option" data-form="novacat" href="#">the Catalog</a> 
						<a class="option" data-form="journal-finder" href="#">Journal Finder</a>
					</div>

					<div class="eightcol last align-right">
						<li class="append field">
							<input class="xwide input" type="search" value="" name="<?php echo $input_name; ?>" id="<?php echo $input_name; ?>" placeholder="<?php echo esc_attr__('Books, journals, and media','bonestheme') ?>" x-webkit-speech speech />
			    			<input class="search-button" type="submit" id="searchsubmit" value="<?php echo esc_attr__('Search') ?>" />
						</li>
					</div>
				    
		    	</ul>

		    </form>

		</div><!--/.wrap-->

	</section><!--/.catalog-->