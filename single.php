<?php get_header(); ?>

	<div id="content">

		<main class="clearfix wrap" id="main" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('col-md--eightcol clearfix ') ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

			    <header class="card__header clearfix" style="margin-top: 1em;">

						<h1 class="beta card__title" itemprop="headline">
							<?php the_title(); ?>
						</h1>

						<?php if ( has_excerpt() ) : ?>
						<p class="delta no-margin"><?php echo get_the_excerpt();?></p>
						<?php endif; ?>

						<div class="byline clearfix zeta hero--small" style="border-bottom: 1px solid #ddd;">
							<div class="col-md--sixcol">
								<em>By</em> <strong style="text-transform: uppercase;"><?php echo get_the_author(); ?></strong>
							</div>

							<div class="col-md--fourcol align-right" style="color: #aaa;">
				   				<?php printf(__('<time class="updated no-margin" datetime="%1$s" pubdate>%2$s</time>', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')) );	?>
				   			</div>

				   			<div class="col-md--twocol">

								<div class="align-right share no-margin">

									<a class="link" onClick="_gaq.push(['_trackEvent', 'Shares', 'Click', 'Facebook']);" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Share on Facebook" target="new"><svg class="svg svg--facebook"><use xlink:href="#icon-facebook"></use></svg></a>

									<a class="link" onClick="_gaq.push(['_trackEvent', 'Shares', 'Click', 'Twitter']);" href="https://twitter.com/intent/tweet?text=<?php the_permalink(); ?>" title="Share on Twitter" target="new">
										<svg class="svg svg--twitter"><use xlink:href="#icon-twitter"></use></svg>
									</a>

								</div>

				   			</div>
						</div>
				    </header> <!-- end article header -->

					<section class="post-content" itemprop="articleBody">
						<?php the_content(); ?>
					</section> <!-- end article section -->

				</article> <!-- end article -->


			<?php endwhile; ?>

			<?php else : ?>

				<article id="post-not-found" class="hentry clearfix">
		    		<header class="article-header">
		    			<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
		    		</header>
		    		<section class="post-content">
		    			<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
		    		</section>
		    		<footer class="article-footer">
		    		    <p><?php _e("This is the error message in the single.php template.", "bonestheme"); ?></p>
		    		</footer>
				</article>

			<?php endif; ?>

		</main> <!-- end #main -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
