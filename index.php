<?php get_header(); ?>

<div class="has-cards" id="content">

	<div class="wrap clearfix">

		<main id="main" class="col-lg--eightcol clearfix hero--small" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('card--alt clearfix'); ?> role="article">

					<?php if ( has_post_thumbnail() ) : ?>
					<div class="media" style="margin-bottom: 1em;">
					<?php the_post_thumbnail('full'); ?>
					</div>
					<?php endif; ?>

					<div class="col-md--threecol clearfix media">

						<p class="small-text">
							<strong style="text-transform: uppercase;"><?php echo get_the_author(); ?></strong>
							<?php
				   				printf(__('<time style="display: block; color: #999;" class="updated" datetime="%1$s" pubdate>%2$s</time>', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')) );
							?>
						</p>

					</div>

					<div class="col-md--ninecol">
						<header class="card__header">
							<a class="link--undecorated" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h2 class="card__title gamma"><?php the_title(); ?></h2></a>
						</header>

						<section class="post-content">

							<?php if ( has_excerpt() ) : ?>
							<p class="epsilon">
								<?php echo get_the_excerpt(); ?>
							</p>
							<?php else : ?>
							<?php the_content(); ?>
							<?php endif; ?>
						</section> <!-- end article section -->
					</div>





				</article> <!-- end article -->

			<?php endwhile; ?>

			    <?php if (function_exists('bones_page_navi')) { // if expirimental feature is active ?>

			        <?php bones_page_navi(); // use the page navi function ?>

		        <?php } else { // if it is disabled, display regular wp prev & next links ?>
			        <nav class="wp-prev-next">
				        <ul class="clearfix">
					        <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
					        <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
				        </ul>
			        </nav>
		        <?php } ?>

		    <?php else : ?>

			    <article id="post-not-found" class="hentry clearfix">
			    	<header class="article-header">
			    		<h1><?php _e("Sorry, No Results.", "bonestheme"); ?></h1>
			    	</header>
			    	<section class="post-content">
			    		<p><?php _e("Try your search again.", "bonestheme"); ?></p>
			    	</section>
			    	<footer class="article-footer">
			    	    <p><?php _e("This is the error message in the search.php template.", "bonestheme"); ?></p>
			    	</footer>
			    </article>

		    <?php endif; ?>

	    </main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
