<?php get_header(); ?>

<div class="has-cards" id="content">

	<header class="has-background background-base hero">
		<div class="col-md--eightcol col--centered clearfix">
			<h1 class="beta"><?php single_cat_title(); ?></h1>
			<p class="no-margin"><?php echo category_description(); ?></p>
		</div>
	</header>

	<div class="wrap clearfix">

		<main id="main" class="col-md--eightcol clearfix hero--small" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <?php if ( has_post_thumbnail() ) : ?>
          <!--<div class="card no-padding media no-margin">
            <?php //the_post_thumbnail( 'medium' ); ?>
          </div>-->
          <?php endif; ?>
          <article class="card">

            <header class="card__header">
              <a href="<?php echo the_permalink(); ?>" class="link link--undecorated">
                <h2 class="card__title delta no-margin"><?php the_title(); ?></h2>
              </a>

              <div class="byline clearfix no-margin zeta">
  							<div class="col-md--eightcol">
  								By <b style="text-transform: capitalize;"><?php echo get_the_author(); ?></b>
  							</div>

  							<div class="col-md--fourcol align-right" style="color: #aaa;">
  				   		<?php printf(__('<time class="updated no-margin" datetime="%1$s" pubdate>%2$s</time>', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')) );	?>
  				   		</div>
  						</div>

            </header>

            <section class="card__content">
            <?php if ( has_excerpt() ) : ?>
            <p class="zeta"><?php echo get_the_excerpt(); ?></p>
            <?php endif; ?>
            </section>

          </article>

			<?php endwhile; ?>

		    <?php else : ?>

			    <article id="post-not-found" class="hentry clearfix">
			    	<header class="article-header">
			    		<h1><?php _e("Sorry, No Results.", "bonestheme"); ?></h1>
			    	</header>
			    	<section class="post-content">
			    		<p><?php _e("Try your search again.", "bonestheme"); ?></p>
			    	</section>
			    	<footer class="article-footer">
			    	    <p><?php _e("This is the error message in the search.php template.", "bonestheme"); ?></p>
			    	</footer>
			    </article>

		    <?php endif; ?>

	    </main> <!-- end #main -->

  <?php if (function_exists('bones_page_navi')) { // if expirimental feature is active ?>

      <?php bones_page_navi(); // use the page navi function ?>

    <?php } else { // if it is disabled, display regular wp prev & next links ?>
      <nav class="wp-prev-next">
        <ul class="clearfix">
          <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
          <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
        </ul>
      </nav>
    <?php } ?>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
