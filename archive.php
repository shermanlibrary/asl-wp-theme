<?php get_header(); ?>

<div class="has-cards"id="content">

	<div id="inner-content" class="wrap clearfix">

	    <div id="main" class="clearfix" role="main">

		    <?php if (is_category()) { ?>
			    <h1 class="epsilon no-margin archive-title">
				    <span><?php _e("Posts Categorized:", "bonestheme"); ?></span> <?php single_cat_title(); ?>
		    	</h1>

		    <?php } elseif (is_tag()) { ?>
			    <h1 class="epsilon no-margin archive-title">
				    <span><?php _e("Posts Tagged:", "bonestheme"); ?></span> <?php single_tag_title(); ?>
			    </h1>

		    <?php } elseif (is_author()) { ?>
			    <h1 class="epsilon no-margin archive-title">
			    	<span><?php _e("Posts By:", "bonestheme"); ?></span> <?php get_the_author_meta('display_name'); ?>
			    </h1>

		    <?php } elseif (is_day()) { ?>
			    <h1 class="epsilon no-margin archive-title">
					<span><?php _e("Daily Archives:", "bonestheme"); ?></span> <?php the_time('l, F j, Y'); ?>
			    </h1>

			<?php } elseif (is_month()) { ?>
    		    <h1 class="epsilon no-margin archive-title">
	    	    	<span><?php _e("Monthly Archives:", "bonestheme"); ?></span> <?php the_time('F Y'); ?>
		        </h1>

		    <?php } elseif (is_year()) { ?>
		        <h1 class="epsilon no-margin archive-title">
		    	    <span><?php _e("Yearly Archives:", "bonestheme"); ?></span> <?php the_time('Y'); ?>
		        </h1>
		    <?php } ?>

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

					<div class="tencol last">
						<header class="article-header">

							<h3 class="search-title delta no-margin"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
			                <p class="byline vcard zeta no-margin">
								<?php the_author_posts_link(); ?>
        					</p>

						</header> <!-- end article header -->
						<section class="post-content">
							<p>
								<?php echo get_the_excerpt(); ?>
							</p>

						</section> <!-- end article section -->
					</div>

					<div class="align-center twocol media first">

						<?php if ( has_post_thumbnail() ) : ?>

							<?php the_post_thumbnail(); ?>

						<?php endif; ?>

						<?php
			   				printf(__('<time style="display: block;" class="updated" datetime="%1$s" pubdate>%2$s</time>', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')) );
						?>

					</div>



				</article> <!-- end article -->

			<?php endwhile; ?>

			    <?php if (function_exists('bones_page_navi')) { // if expirimental feature is active ?>

			        <?php bones_page_navi(); // use the page navi function ?>

		        <?php } else { // if it is disabled, display regular wp prev & next links ?>
			        <nav class="wp-prev-next">
				        <ul class="clearfix">
					        <li class="prev-link"><?php next_posts_link(_e('&laquo; Older Entries', "bonestheme")) ?></li>
					        <li class="next-link"><?php previous_posts_link(_e('Newer Entries &raquo;', "bonestheme")) ?></li>
				        </ul>
			        </nav>
		        <?php } ?>

		    <?php else : ?>

			    <article id="post-not-found" class="hentry clearfix">
			    	<header class="article-header">
			    		<h1><?php _e("Sorry, No Results.", "bonestheme"); ?></h1>
			    	</header>
			    	<section class="post-content">
			    		<p><?php _e("Try your search again.", "bonestheme"); ?></p>
			    	</section>
			    	<footer class="article-footer">
			    	    <p><?php _e("This is the error message in the search.php template.", "bonestheme"); ?></p>
			    	</footer>
			    </article>

		    <?php endif; ?>

		</div> <!-- end #main -->

		<?php get_sidebar(); // sidebar 1 ?>

    </div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
