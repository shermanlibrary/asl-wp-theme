
		</div> <!-- end #container -->

		<footer id="footer" class="footer" role="contentinfo">

	        <div id="inner-footer" class="wrap clearfix">

                <div class="col-sm--twelvecol col-lg--sixcol media">
                    <a href="//www.nova.edu/library/main/" title="Library Homepage"><img src="http://sherman.library.nova.edu/cdn/media/images/joint-use-logo-in-color.png" alt="The Alvin Sherman Library is a joint-use facility between Nova Southeastern University and the Broward County Board of County Commissioners"></a>
	            </div>

	            <div class="col-sm--twelvecol col-lg--sixcol align-right">

	            	<div class="col-sm--twelvecol col-md--twocol col-lg--twelvecol no-margin share">
						<a class="link" onclick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Facebook']);" href="//www.facebook.com/AlvinShermanLibrary" title="Alvin Sherman Library Facebook Page" alt="We are on Facebook"><svg class="svg svg--facebook" style="fill: #fff;"><use xlink:href="#icon-facebook"></use></svg></a>
						<a class="link" onclick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Twitter']);" href="//www.twitter.com/shermanlibrary" title="Alvin Sherman Library on Twitter" alt="We are on Twitter">
							<svg class="svg svg--twitter" style="fill: #fff;"><use xlink:href="#icon-twitter"></use></svg>
						</a>
					</div>

		            <p class="col-sm--twelvecol col-md--fivecol col-lg--twelvecol no-margin zeta">
		                <a class="link link--undecorated" href="#feedback" onclick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Contact Form']);">Contact</a> /
		                <a class="link link--undecorated" href="//sherman.library.nova.edu/sites/directions/" onclick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Directions']);">Directions</a> /
		                <a class="link link--undecorated" href="//sherman.library.nova.edu/sites/hours/" onclick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Hours']);"> Hours</a> /
		                <a class="link link--undecorated" href="//www.nova.edu/library/staffonly/" onclick="_gaq.push(['_trackEvent', 'Footer', 'Click', 'Staff']);"> Staff</a>
		            </p>
	            </div>

	        </div> <!-- end #inner-footer -->

	    </footer> <!-- end footer -->


		<section class="modal semantic-content" id="feedback" tabindex="-1" role="dialog" aria-labelledby="modal-label" aria-hidden="false">

		    <div class="modal-inner">

		        <header id="modal-label">
		            <h3>Contact Us</h3>
		        </header>

		        <div class="modal-content clearfix">

		        	<p class="zeta">
		        		Hi there. Drop us a line &mdash; feedback, questions, or great ideas &mdash; and
		        		we will get back to you as soon as we can <small>(usually 24 to 48 hours)</small>.
		        	</p>

					<form class="form" action="http://systems.library.nova.edu/form/embed.php?#main_body" id="form_49" method="post">

						<ul>

							<li class="form__field">
								<textarea class="form__input form__input--full-width textarea" ="element_3" name="element_3" required="" placeholder="I think it would be great if ...."></textarea>
							</li>

							<li class="form__field">
								<label class="form__label" for="element_1">Name</label>
								<input class="form__input form__input--full-width" type="text" id="element_1" name="element_1" placeholder="Jane Doe" required="">
							</li>


							<li class="form__field">
								<label class="form__label" for="element_2">Email address</label>
								<input class="form__input form__input--full-width" type="email" id="element_2" name="element_2" required="" placeholder="jdoe@nova.edu">
							</li>

							<li class="align-right">
								<input type="hidden" name="form_id" value="49">
								<input type="hidden" name="submit" value="1">
								<input class="button button--primary button--flat button--small zeta" id="saveForm" name="submit" onclick="" type="submit" value="Submit">
							</li>
						</ul>
						<input type="hidden" name="as_sfid" value="AAAAAAU_mOX4jlzsBtJYxjZTDjMoNolbXJpA3jKoBJGz3Zhp-Zgz2P0GTA19bOl6tgXWalY8NbHToBNzSJgaUocXqa9XPxs_eGKB4Tq3-LTqCPS-Tw==" /><input type="hidden" name="as_fid" value="UFC-igI0z7OtvWyFiqik" />
					</form>

					<p class="align-center no-margin zeta">
						<a href="//sherman.library.nova.edu/sites/contact">Phone Numbers</a> /
						<a href="//sherman.library.nova.edu/sites/hours">Desk and Services Hours</a> /
						<a href="//www.nova.edu/library/main/ask.html">Ask a Librarian</a>
					</p>

		        </div>
		    </div>

		    <a href="#!" class="modal-close" title="Close this modal" data-close="Close" data-dismiss="modal">×</a>

		</section>

		<script data-main="//sherman.library.nova.edu/cdn/scripts/config.js" src="//sherman.library.nova.edu/cdn/scripts/require-2.1.20.min.js"></script>
        <script src="//sherman.library.nova.edu/cdn/scripts/min/ng-route-sanitize-cookies-1.5.7.min.js"></script>
        <script src="//sherman.library.nova.edu/assets/js/apps/application--wordpress.js"></script>

		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html> <!-- end page. what a ride! -->
