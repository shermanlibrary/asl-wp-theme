<?php
/*
 * Template Name: Content with Sidebar Menu
 * Description: The main content is on the right, with a dynamic menu on the left.
 */

?>

<?php get_header(); ?>

	<div id="content">

		    <main class="hero" id="main" role="main">

			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

					<h1 class="hide-accessible" itemprop="headline">
						<?php the_title(); ?>
					</h1>

					<div class="clearfix wrap">

						<nav class="col-md--fourcol sidebar"></nav>

						<div class="col-md--eightcol col--last">
						    <section class="post-content" itemprop="articleBody">
							    <?php the_content(); ?>
							</section> <!-- end article section -->

						    <footer class="article-footer wrap clearfix">

							    <?php the_tags('<p class="tags"><span class="tags-title">Tags:</span> ', ', ', '</p>'); ?>

						    </footer> <!-- end article footer -->
					    </div>



				    </div>

			    </article> <!-- end article -->

			    <?php endwhile; ?>

			    <?php else : ?>

				    <article id="post-not-found" class="hentry clearfix">
				    	<header class="article-header">
				    		<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
				    	</header>
				    	<section class="post-content">
				    		<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
				    	</section>
				    	<footer class="article-footer">
				    	    <p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
				    	</footer>
				    </article>

			    <?php endif; ?>

			</main> <!-- end #main -->

		    <?php //get_sidebar(); ?>

	</div> <!-- end #content -->

<?php get_footer(); ?>
