<?php
/*
 * Template Name: Full-Width Blank Slate
 * Description: The content is a full-width blank slate to accommodate custom layouts using HTML.
 */

?>

<?php get_header(); ?>

<style type="text/css">

.has-cards {
	background-color: #f5f5f5;
}
.no-padding {
	padding: 0;
}

.form__submit--inside {
	position: absolute;
	right: 0;
	top: -.5em;
}

.hero--small {
	padding-bottom: 1em;
	padding-top: 1em;
}

</style>
	
	<div id="content">
				
		    <div class="clearfix" id="inner-content" style="padding: 0;">

			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			   	    <?php 
			   	    $content = apply_filters( 'the_content', get_the_content() );
			   	    $content = str_replace( ']]>', ']]&gt;', $content );

			   	    echo $content;
			   	    ?>
				
			    <?php endwhile; ?>		
			
			    <?php else : ?>
			
				    <article id="post-not-found" class="hentry clearfix">
				    	<header class="article-header">
				    		<h1><?php _e("Oops, Post Not Found!", "bonestheme"); ?></h1>
				    	</header>
				    	<section class="post-content">
				    		<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "bonestheme"); ?></p>
				    	</section>
				    	<footer class="article-footer">
				    	    <p><?php _e("This is the error message in the page.php template.", "bonestheme"); ?></p>
				    	</footer>
				    </article>
			
			    <?php endif; ?>
	
			</div> <!-- end #inner-content -->

		    
	</div> <!-- end #content -->

<?php get_footer(); ?>