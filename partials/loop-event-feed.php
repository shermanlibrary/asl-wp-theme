<?php

/* ==================
 * Event Feed, Agenda View
 * ================== */
// Build the API
$spotlight_taxonomy_api_url 	= 'http://sherman.library.nova.edu/sites/spotlight/api/taxonomy/get_taxonomy_posts/';
$spotlight_taxonomy 			= $tax;
$taxonomy_slug					= $slug;
$count 							= 0;

$json = json_decode(
	wp_remote_retrieve_body(
	 	wp_remote_get(
		 	$spotlight_taxonomy_api_url . '?taxonomy=' .
		 	$spotlight_taxonomy .'&slug=' .
		 	$taxonomy_slug . '&post_type=spotlight_events&count=100',

		  	array( 'sslverify' => false)
	  	)
	), true);

$term_title = $json['term']['title'];

if ( !function_exists( 'rearrange' ) ) {
	function rearrange( $a, $b ) { return strcmp( $a['custom_fields']['event_start'][0], $b['custom_fields']['event_start'][0]); }
}
usort( $json['posts'], 'rearrange');

foreach ( $json['posts'] as $response) :

		// Date and Time
		$allday			= false;
		$multiday		= false;
		$date_start		= $response['custom_fields']['event_start'][0];
		$date_options	= $response['custom_fields']['scheduling_options'][0];
		$start 			= '';
		$end 			= '';

		if ( !$date_options ) {
			$event_start_time 	= $response['custom_fields']['event_start_time'][0];
			$event_end_time 	= $response['custom_fields']['event_end_time'][0];

			$start 				= $date_start . $event_start_time;
			$end 				= $date_start . $event_end_time;
		}

		else {

			if ( strpos( $date_options, 'multiday' ) !== false ) {
				$start = $date_start;
				$multiday = true;

				if ( strpos( $date_options, 'allday' ) !== false ) {

					$allday = true;
					$end = $response['custom_fields']['event_end'][0];

				} else {

					$end = $response['custom_fields']['event_end'][0] . $response['custom_fields']['event_end_time'][0];
				}
			}

		}

		// Content
		$excerpt		=	$response['excerpt'];
		$event_type		=	( $response['taxonomy_event_type'] ? $response['taxonomy_event_type'][0] : null );
		$location		=	$response['taxonomy_location'][0];
	?>

<?php

	if ( strtotime($end) > time() ) :
		$count ++;
		if ( $count <= $num ) :
?>

<article class="card clearfix" itemscope itemtype="http://schema.org/Event" role="article">

	<span class="card__color-code"></span>

	<header class="card__header" style="margin-bottom: .5em;">
		<a class="link--undecorated _link_blue" href="<?php echo $response['url']; ?>" itemprop="url">
			<h2 class="menu__item__title" style="margin-bottom: .25em;"><?php echo $response['title']; ?></h2>
		</a>
		<p class="no-margin small-text">
			<time class="time" datetime="<?php echo $start . ( $multiday === true ? '-' . $end : ''); ?>">
				<span itemprop="startDate" content="<?php echo $start ?>"><b><?php echo date('F jS', strtotime($start) ); ?></b></span> <?php echo ( $multiday === false ? '' : '<span class="zeta">through <span itemprop="endDate" content="' . $end . '">' . date('F jS', strtotime($end) ) . '</span></span>' ); ?>

				<?php if ( !$allday || !$multiday ) : ?>
				<span class="small-text time__hours" style="color: #999;">
					<?php echo date( 'g:i a', strtotime( $start ) ); ?> - <?php echo date('g:i a', strtotime( $end )); ?>
				</span>
				<?php endif; ?>

			</time>
		</p>
		</a>
	</header>

	<section class="content">
		<p class="no-margin">
			<?php echo ( $excerpt ? strip_tags( $excerpt ) : null ); ?>
		</p>
	</section>

</article>


	<?php endif;?>
<?php endif;?>
<?php endforeach; ?>

<?php if ( $count === 0 ) : ?>

	<?php  if ( $tax === 'series' ) : ?>
		<p> There are no upcoming <?php echo '<a href="http://sherman.library.nova.edu/sites/spotlight/' . $spotlight_taxonomy . '/' . $taxonomy_slug . '/">' . $term_title . '</a>' ?> at this time. </p>

	<?php  elseif ( $tax === 'location' ) : ?>
		<p>There are no programs currently scheduled.</p>
	<?php endif; ?>
<?php endif; ?>
